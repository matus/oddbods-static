var _STRINGS = {
	"Ad":{
		"Mobile":{
			"Preroll":{
				"ReadyIn":"The game is ready in ",
				"Loading":"Your game is loading...",
				"Close":"Close",
			},
			"Header":{
				"ReadyIn":"The game is ready in ",
				"Loading":"Your game is loading...",
				"Close":"Close",
			},
			"End":{
				"ReadyIn":"Advertisement ends in ",
				"Loading":"Please wait ...",
				"Close":"Close",
			},
		},
	},

	"Splash":{
		"Loading":"Loading ...",
		"LogoLine1":"Some text here",
		"LogoLine2":"powered by MarketJS",
		"LogoLine3":"none",
	},

	"Game":{
		"SelectPlayer":"Select Player",
        "You":"YOU",
		"Win":"WIN",
		"Lose":"LOSE",
		"Score":"Score",
        "Highscore":"Highscore",
        "Coin":"Coin",
		"Time":"Time",
        "Music":"MUSIC",
		"Sound":"SOUND",
        "Paused":"PAUSED",
        "Game":"GAME",
        "Over":"OVER",
        "Settings":"SETTINGS",
        "Levels":"LEVELS",
        "ArmoCombo":"Extras Combo",
        "PopCombo":"Pop Combo",
        "DropCombo":"Drop Combo",
        "level":"level:",
        "score":"score:",
        "READY":"READY",
        "GO":"GO",
        "Proceed":"tap to proceed",
	},

	"Results":{
		"Title":"High score",
	},



};

var _LVL = {




    "lvl0":{

        "colourSet": [["green", "yellow"]],
        "armo": [10,10,0,0],//old armo defination for each colour armo now used to know whether the armo colour is given
        "armoEachArmo":10,//how many armo for each colour
        "armoExtra": 1,
        "starScore":[2000,3000,3312],
        "rainbow":0,
        "arrow":[{x:108,y:354},{x:108,y:354},{x:121,y:615}],
        "rowOffset":0,//0 to offset odd rows, 1 to offset even rows
        "speech":["My pet bugs are trapped in these bubbles!","Let's pop these bubbles and rescue them!","Click on the arrow button or press B to use the floating bubble"],
        "speechMobile":["My babies are trapped in these bubbles!","Let's pop these bubbles and rescue them!","Click on the arrow button to use the floating bubble"],
        "map":[
			[,0,,0,0,0,0,,0,],
            [,0,0,,0,"babycockroach",0,,0,0,],
            [0,"babycockroach",0,,0,0,,0,"babycockroach",0],
            [0,0,0,0,,0,,0,0,0,0],
            [,1,,1,1,1,1,,1,],
            [,1,1,,1,1,1,,1,1,],
            [1,1,1,,1,1,,1,1,1],
            [1,1,1,1,,1,,1,1,1,1],
            [,0,,0,0,0,0,,0,],
            [,0,0,,0,"babycockroach",0,,0,0,],
            [0,"babycockroach",0,,0,0,,0,"babycockroach",0],
            [0,0,0,0,,0,,0,0,0,0]
        ]
        },
    "lvl1":{
        "colourSet": [["purple", "red", "green", "pink"]],
        "armo": [5,5,5,5],
        "armoEachArmo":6,
        "armoExtra": 1,
        "starScore":[1700,2816,3349],
        "rainbow":0,
        "rowOffset":0,//0 to offset odd rows, 1 to offset even rows
        "map":[
			 [1,1,,,0,0,,,2,2,],
			 [1,1,1,,0,"babyjeff",0,,2,2,2],
			 [1,1,3,3,0,0,3,3,2,2,],
			 [,,,3,,,,3,,,],
			 [0,0,3,3,2,2,3,3,0,0,],
			 [0,"babyjeff",0,,2,2,2,,0,"babyjeff",0],
			 [0,0,3,3,2,2,3,3,0,0,],
			 [,,,3,,,,3,,,],
			 [2,2,3,3,0,0,3,3,1,1,],
			 [2,2,2,,0,"babyjeff",0,,1,1,1],
			 [2,2,,,0,0,,,1,1,]
        ]
        },
    "lvl2":{

        "colourSet": [["red","yellow","pink","purple"]],
        "armo": [13,13,13,13],
         "armoEachArmo":13,
        "starScore":[3000,4500,6000],
        "armoExtra": 1,
        "rainbow":0,
        "rowOffset":0,//0 to offset odd rows, 1 to offset even rows
        "arrow":{x:109,y:284},
        "speech":["Avoid hitting the - bubbles!","You'll lose 3 bubble ammo for popping one of those!"],
        "map":[
			 [2,"babybear",2,3,3,2,"babybear",2,1,1],
			 [2,2,"minus3","minus3",1,3,2,2,2,1,1],
			 [0,0,0,1,1,1,3,2,2,3],
			 [3,0,0,2,1,1,1,3,3,0,3],
			 [3,3,3,"minus2","minus2",1,1,3,0,3],
			 [1,3,3,"minus2","minus2",3,0,"minus0","minus0",0,3],
			 [1,1,1,1,3,3,"babybear","minus0",1,3],
			 [3,1,1,3,3,3,2,2,1,1,1],
			 [3,2,"babybear",2,2,1,2,2,2,2],
			 ["minus3","minus3",2,1,2,1,3,3,"minus3",0,2],
			 [1,"minus3",3,1,1,1,2,"minus3",3,0],
			 [1,1,0,0,3,3,1,"babybear",0,0,0],
			 [1,"babybear",0,0,0,3,3,2,2,2],
			 [1,2,2,2,0,0,3,3,3,2,2]
        ]
        },
        "lvl3":{
        "colourSet": [["green","yellow","orange","purple"]],
        "armo": [15,15,15,15],
        "armoEachArmo":15,
        "starScore":[3000,4347,5000],
        "armoExtra": 1,
        "rainbow":0,
        "rowOffset":0,//0 to offset odd rows, 1 to offset even rows
        "map":[
			[,,1,1,,,1,1,,],
			[,,,"babybub",,,,"babybub",,,],
			[,,0,3,,,3,0,,],
			[,,3,0,2,,2,0,3,,],
			[,3,0,1,2,2,1,0,3,],
			[3,2,2,1,0,"babybub",0,1,2,2,3],
			[3,3,2,1,0,0,1,2,3,3],
			[1,2,1,0,2,1,2,0,1,2,1],
			[1,2,2,3,1,1,3,3,3,1],
			[2,3,1,0,1,3,1,2,2,0,3],
			[2,3,1,0,3,2,3,2,0,3],
			[0,2,2,3,3,0,2,3,1,1,3],
			[0,0,1,1,1,0,1,1,3,1],
			[2,1,3,0,3,1,0,0,2,2,3],
			[3,0,1,3,,1,2,3,1,3],
			[0,0,3,3,,,1,2,2,1,1]]
        },
    "lvl4":{
        "colourSet": [["green","yellow","orange","purple"]],
        "armo": [13,13,13,13],
        "armoEachArmo":13,
        "starScore":[2000,3252,4000],
        "armoExtra": 1,
        "rainbow":0,
        "rowOffset":0,//0 to offset odd rows, 1 to offset even rows
        "speech":["Bubble block can't be popped!", "Drop them to get rid of them!"],
        "arrow":{x:88,y:356},
        "map":[
			[,0,"babyslick",0,,,0,"babyslick",0,],
			[,,2,2,,,,2,2,,],
			[,,"wood",,,,,"wood",,],
			[,,2,2,2,"babyslick",3,3,3,,],
			[,0,"wood",1,0,3,2,"wood",2,],
			[,0,"wood",1,3,0,1,2,"wood",3,],
			[3,"wood",3,"wood",1,2,"wood",2,"wood",1],
			[,3,"wood",3,0,0,3,0,"wood",0,],
			[1,2,2,"wood",1,3,"wood",1,3,0],
			[1,"wood",0,3,2,1,0,1,2,"wood",3],
			[0,3,,,,,,,2,0]
        ]
        },
     "lvl5":{
        "colourSet": [["blue","pink","purple","green"]],
        "armo": [10,10,10,10],
        "armoEachArmo":13,
        "starScore":[3900,4403,5075],
        "armoExtra": 1,
        "rainbow":0,
        "rowOffset":0,//0 to offset odd rows, 1 to offset even rows
        "speech":["Try popping bubbles beside the clear bubbles","They take over the colour of the popped bubble"],
        "arrow":{x:89,y:354},
        "map":[
         [3,2,"babygoat",0,3,2,1,"babygoat",3,2],
         [0,3,2,1,0,3,2,1,0,3,2],
         [0,3,2,1,0,3,2,1,0,3],
         ["ice","ice","babygoat","ice","ice","ice","ice","ice","babygoat","ice","ice"],
         [0,3,2,1,0,3,2,1,0,3],
         [1,0,3,2,1,0,3,2,1,0,3],
         [1,0,3,2,1,0,3,2,1,0],
         ["ice","ice","ice","babygoat","ice","ice","ice","babygoat","ice","ice","ice"],
         [1,0,3,2,1,0,3,2,1,0],
         [2,1,0,3,2,1,0,3,2,1,0],
         [2,1,0,3,2,1,0,3,2,1],
         ["ice","ice","babygoat","ice","ice","ice","ice","ice","babygoat","ice","ice"],
         [2,1,0,3,2,1,0,3,2,1]
        ]
        },
    "lvl6":{
        "colourSet": [["red","yellow","pink","purple"]],
        "armo": [13,13,13,13],
        "armoEachArmo":13,
        "starScore":[3100,4000,4900],
        "armoExtra": 1,
        "rainbow":0,
        "rowOffset":0,//0 to offset odd rows, 1 to offset even rows
         "map":[
			 [,,1,3,3,2,3,1,,],
			 [,,,0,2,"babybird",3,0,,,],
			 [,,,0,0,2,0,,,],
			 [,,,1,1,1,0,0,,,],
			 [,,,2,2,2,2,,,],
			 [,,,"wood",0,"babybird",1,1,,,],
			 [,,,0,3,1,"wood",,,],
			 [,,,"ice","ice","ice","ice","ice",,,],
			 [,,,0,"wood",1,3,,,],
			 [,,,0,0,1,2,3,,,],
			 [,,,3,1,2,3,,,],
			 [,,,3,1,2,0,0,,,],
			 [,,,3,1,0,0,,,],
			 [,,,"ice","ice","babybird","ice","ice",,,],
			 [,,,0,"wood",1,3,,,],
			 [,,,0,0,1,2,3,,,],
			 [,,,3,1,2,3,,,],
			 [,,,3,1,2,0,0,,,],
			 [,,,3,1,0,0,,,],
			 [,,,"ice","ice","babybird","ice","ice",,,],
			 [,1,0,1,3,3,1,0,2,],
			 [,,1,0,1,3,1,0,2,,],
			 [,,1,0,3,1,0,2,,],
			 [,,,3,3,2,2,2,,,],
			 [,,,0,0,1,3,,,],
			 [,,,,0,3,3,,,,],
			 [,,,,0,3,,,,],
			 [,,,,,3,,,,,]
        ]
        },
//redo
    "lvl7":{
        "colourSet": [["green","yellow","orange","purple"]],
        "armo": [13,13,13,13],
        "armoEachArmo":16,
        "starScore":[2500,3800,4300],
        "armoExtra": 1,
        "rainbow":0,
        "rowOffset":0,//0 to offset odd rows, 1 to offset even rows
        "map":[
			 [,,,,"ice","ice",,,,],
			 [,"ice","ice",,"ice",2,"ice",,"ice","ice",],
			 ["ice","babyalien","ice","ice","ice","ice","ice","ice","babyalien","ice"],
			 [,"ice","ice",,"ice","ice","ice",,"ice","ice",],
			 ["ice",3,"ice","ice",0,2,"ice","ice",1,"ice"],
			 [,"ice","ice",,0,"babyalien",2,,"ice","ice",],
			 ["ice",2,"ice","ice",1,3,"ice","ice",0,"ice"],
			 [,"ice","ice","ice","ice","ice","ice","ice","ice","ice",],
			 ["ice","ice","ice",3,"ice","ice",1,"ice","ice","ice"],
			 ["ice","ice",1,"ice","ice","ice","ice","ice",2,"ice","ice"],
			 ["ice","ice","ice",0,"ice","ice",3,"ice","ice","ice"],
			 ["ice","ice","babyalien","ice","ice",3,"ice","ice","babyalien","ice","ice"],
			 ["ice","ice","ice","ice",0,2,"ice","ice","ice","ice"],
			 ["ice",3,"ice",0,"ice","ice","ice",2,"ice",1,"ice"]
        ]
        },
    "lvl8":{
        "colourSet": [["green","yellow","orange","purple"]],
        "armo": [0,15,15,15],
        "armoEachArmo":15,
        "starScore":[1100,2000,3000],
        "armoExtra": 1,
        "rainbow":0,
        "rowOffset":0,//0 to offset odd rows, 1 to offset even rows
        "speech":["Popping each + bubble will give you 3 extra bubbles"],
        "arrow":{x:72,y:318},
        "map":[
			 [1,1,,,3,3,,,3,3],
			 [2,"babyrobot",3,,2,"babyrobot",1,,2,"babyrobot",1],
			 ["minus2",3,,,"minus2","plus1",,,2,"minus1"],
			 [,"babyrobot",,,,"babyrobot",,,,"babyrobot",],
			 ["plus2",2,,,3,1,,,"plus1",1],
			 [3,"babyrobot",2,,"plus3","babyrobot","minus1",,1,"babyrobot","minus2"],
			 [3,"plus3",,,3,"minus1",,,"minus2",2],
			 [,"babyrobot",,,,"babyrobot",,,,"babyrobot",],
			 [3,"minus3",,,1,1,,,2,2],
			 ["plus2","babyrobot","minus3",,3,"babyrobot",2,,"plus2","babyrobot","minus3"],
			 [2,2,,,3,2,,,3,3],
			 [,"babyrobot",,,,"babyrobot",,,,"babyrobot",],
			 ["plus2",2,,,3,2,,,"minus3",3],
			 [1,"babyrobot",2,,"minus3","babyrobot","minus2",,3,"babyrobot",1],
			 [1,1,,,1,1,,,1,1]
        ]
       },
	   
	"lvl9":{
        "colourSet": [["blue","pink","purple","green"]],
        "armo": [13,13,13,0],
        "armoEachArmo":16,
        "starScore":[2800,4000,6000],
        "armoExtra": 1,
        "rainbow":0,
        "rowOffset":0,//0 to offset odd rows, 1 to offset even rows
        "arrow":{x:241,y:385},
        "speech":["Hitting the thunder bubble will pop the whole row of bubbles!"],
        "map":[
			 [1,1,"babyrabbit",1,1,"wood","wood","thunder","wood","wood"],
			 [2,2,0,0,1,1,0,"ice","ice",0,0],
			 [1,0,"ice",2,1,2,2,"ice",2,2],
			 [0,1,"ice","ice",1,0,2,0,"babyrabbit",1,0],
			 [0,0,2,2,0,1,0,1,2,0],
			 ["wood","wood","thunder","wood","wood",2,2,"ice","ice",2,0],
			 [0,"ice","ice","babyrabbit",0,2,0,2,1,0],
			 [0,2,"ice",1,2,1,1,0,2,"babyrabbit",1],
			 [2,0,0,0,2,2,1,1,0,0],
			 [2,1,"ice","ice",1,1,"wood","wood","thunder","wood","wood"],
			 [0,"babyrabbit",0,2,0,1,1,"ice","ice",0],
			 [0,0,1,2,0,0,2,0,"ice",0,0],
			 [0,2,0,2,1,0,2,2,2,0],
			 ["wood","wood","wood","wood","wood","thunder","wood","wood","wood","wood","wood"],
        ]
        },
	"lvl10":{
        "colourSet": [["blue","pink","purple","green"]],
        "armo": [16,16,16,16],
        "armoEachArmo":16,
        "starScore":[2000,3000,4200],
        "armoExtra": 1,
        "rainbow":0,
        "rowOffset":0,//0 to offset odd rows, 1 to offset even rows
        "speech":["Avoid hitting skull bubbles at all costs!","Otherwise you'll lose the game!"],
        "arrow":{x:127,y:182},
        "map":[
         ["ice","ice","babyduck","ice","ice","ice","ice","ice","babyduck","ice"],
         [,1,2,"evil",0,,1,"evil",2,3,],
         [,1,2,0,,,1,2,3,],
         [,,0,1,,,,3,0,,],
         [1,0,2,1,1,3,3,2,1,1],
         [1,3,2,0,"babyduck","evil",2,0,"babyduck",2,1],
         [,1,3,3,0,2,0,1,2,],
         [,1,"evil",2,3,,2,1,"evil",0,],
         [,1,2,,,,,3,0,],
         [,,"ice",,,,,,"ice",,],
         [1,"ice",3,2,1,2,1,0,"ice",3],
         ["ice","babyduck",3,"ice",1,0,1,"ice",2,"babyduck","ice"],
         ["ice",0,1,"ice",2,0,"ice",1,3,"ice"],
         [0,1,2,3,0,1,2,3,0,1,2]
        ]
        },
		
	"lvl11":{
        "colourSet": [["red","yellow","pink","purple"]],
        "armo": [13,13,13,13],
        "armoEachArmo":13,
        "starScore":[1800,3000,4000],
        "armoExtra": 1,
        "rainbow":0,
        "rowOffset":0,//0 to offset odd rows, 1 to offset even rows
        "speech":["Shoot special bubble to pop all bubbles with the same colour as your shooting bubble"],
        "arrow":{x:243,y:183},
        "map":[
			 [0,0,,2,2,1,1,,2,2],
			 [1,"star",3,3,"evil","evil","evil",0,0,"star",3],
			 [1,3,,"evil","babykoala","babykoala","evil",,0,3],
			 [,,,"evil","babykoala","babykoala","babykoala","evil",,,],
			 [,3,3,"evil","babykoala","babykoala","evil",0,0,],
			 [,,,3,"evil","evil","evil",0,,,],
			 [,,,,1,1,,,,],
			 [3,3,"evil",2,2,"star",2,2,"evil",3,3],
			 [1,0,0,3,3,3,3,1,1,0],
			 [1,0,1,2,1,2,0,"ice",0,1,3],
			 [2,2,0,1,0,1,"evil","ice",2,3],
			 [2,1,"ice","evil","ice",3,2,1,3,2,2],
			 [1,2,3,1,2,3,2,1,0,3],
			 [0,0,3,2,3,2,1,0,2,1,1]
        ]
        },
};
