(function ($, root, undefined) {

	$(function () {
		'use strict';
		// DOM ready, take it away

		//if on video page
		if ($('div.video-container').length) {

			//reference to image
			var bgImage = $('img.video-background')

			//reference to frame
			var frame = $('div.video-holder')

			//listen to browser size changes and reposition player
			addResizeListener(function () {
				adjustVideoSizeAndPosition(bgImage, frame)
			})

			//this happens on image loaded - needed for safari
			bgImage.one("load", function () {
				adjustVideoSizeAndPosition(bgImage, frame)
			})

			//this happens on DOM ready
			adjustVideoSizeAndPosition(bgImage, frame)
		}

		//if on play game page
		if ($('div.game-container').length) {
			var gameBg = $('div.game-background')

			//listen to browser size changes and reposition player
			addResizeListener(function () {
				adjustGameSize(gameBg)
			})

			//this happens on DOM ready
			adjustGameSize(gameBg)
		}

		//if on buy here page
		//if toys range lightbox
		if ($('#lightbox-toys').length) {
			var $lb = $('#lightbox-toys')

			//listen to browser size changes and reposition player
			addResizeListener(function () {
				adjustLightboxSize($lb)
			})

			//this happens once, on DOM ready
			adjustLightboxSize($lb)

		}


	});

	/**
	 * Adds function that is called on resize
	 *
	 * @param callback
	 */
	function addResizeListener(callback) {
		var resizeDebounced = debounce(callback, 200, false)
		$(window).resize(resizeDebounced)
	}

	/**
	 * Adjust frame position so it matches the size of the
	 * underlying video-background image.
	 *
	 * The video-bg image is resized by browser (max-width: 100%)
	 *
	 * @param $bgImage Is jQuery object, reference to bg image
	 */
	function adjustVideoSizeAndPosition($bgImage, $frame) {

		var imageWidth = 1154 //original image width
		var imageHeight = 654 //original image height
		var videoWidth = 950  //original video frame width
		var videoHeight = 530 ////original video frame height

		var relativeWidth = videoWidth / imageWidth
		var relativeHeight = videoHeight / imageHeight

		var newWidth = relativeWidth * $bgImage.width()
		var newOffsetX = ($bgImage.width() - newWidth) * 0.5

		var newHeight = relativeHeight * $bgImage.height()
		var newOffsetY = ($bgImage.height() - newHeight) * 0.5

		$frame.css({
			display: 'block',
			width: newWidth,
			height: newHeight,
			left: newOffsetX,
			top: newOffsetY
		})
	}

	/**
	 * Resize the game background (and so its content with game)
	 * so it matches the browser width. Do not resize above original
	 * size.
	 *
	 * @param $gameBg Is jQuery reference to game-background div
	 */
	function adjustGameSize($gameBg) {
		var originalWidth = 482;
		var originalHeight = 629;
		var ratio = originalWidth / originalHeight;
		var margins = 2 * 10;

		var browserWidth = $(window).width() - margins;

		var newWidth, newHeight;

		if (browserWidth < originalWidth) {
			newWidth = browserWidth
			newHeight = browserWidth / ratio
		}
		else {
			newWidth = originalWidth
			newHeight = originalHeight
		}

		//do not resize in landcape mode
		if ($('div.landscape-rotate-graphics').css('display') == 'block') {
			return;
		}
		else {

			$gameBg.css({
				width: newWidth,
				height: newHeight,
			})
		}
	}

	/**
	 * Resizes the lightbox size so it matches the browser window
	 *
	 * @param $lb
	 * @param callback Is function to trigger after lb is resized
	 */
	function adjustLightboxSize($lb, callback) {

		var browserWidth = $(window).width();
		var browserHeight = $(window).height();
		var maxSize = 700;
		var margin = 20; //margin on one side
		var size = Math.min(browserWidth, browserHeight)

		//on small screens, make the lightbox the size of window minus margin
		if ((size - 2 * margin) < maxSize) {
			size = size - 2 * margin
		}

		size = Math.min(maxSize, size)

		$lb.css({
			width: size,
			height: size,
		})

		if (callback) {
			callback()
		}
	}

	/**
	 * Lightbox default settings
	 *
	 * @link https://github.com/noelboss/featherlight/#content-filters
	 */
	$.featherlight.defaults.closeIcon = '';  //we are using own specified in featherlite.css
	$.featherlight.defaults.afterOpen = initToysSliders //


	/**
	 * Launched after Lightbox is opened
	 *
	 * @param e Is the event that triggered the lightbox
	 */
	function initToysSliders(e) {

		var buttonId = $(e.currentTarget).attr('id')

		//if buttonToys was clicked
		if (buttonId == 'button-toys') {

			//init button events
			setupToysLightboxButtons();
			
			$carousel = $('div.featherlight-content .owl-carousel')

			//init owls carousels
			$carousel.owlCarousel({
				items: 1,
				slideSpeed: 800,
				autoPlay: true,
				stopOnHover: true,
				navigation: true,
				pagination: false,
				navigationText: [
					"",
					""
				],
				singleItem: true
			});


			//resize menu buttons
			if (Modernizr.flexwrap && Modernizr.flexbox) {
				// supported
			} else {
				// not-supported
				$('div.featherlight-content div.menu').css({display: 'block'});
				var menuWidth = $('div.featherlight-content div.menu').width() - 2;

				var menuItemWidth = Math.floor(menuWidth/3);
				$('div.featherlight-content div.menu button').each(function(index, btn){
					$(btn).css({ width: menuItemWidth});
				});
			}

			//select a tab, must be after init owl carousels...
			selectTab(oddbods.currentTab);
		}

	}

	/**
	 * Assings click events to the menu buttons
	 */
	function setupToysLightboxButtons() {
		$('div.featherlight-content button[data-tab]').click(function (e) {
			var tab = ($(this).data('tab'));
			selectTab(tab)
		})
	}


	function selectTab(tabId) {
		//save active tab
		oddbods.currentTab = tabId

		//select active button, hide others
		$('div.featherlight-content button[data-tab]').each(function (index, btn) {
			var $btn = $(btn)
			$btn.removeClass('active');
			if ($btn.data('tab') == tabId) {
				$btn.addClass('active');
			}
		})

		var $activeSlider

		//select active slider, hide others
		$('div.featherlight-content .owl-carousel').each(function (index, slider) {
			var $slider = $(slider);

			if ($slider.data('tab') == tabId) {
				$slider.show();
				$activeSlider = $slider;
			}
			else {
				$slider.hide();
			}
		})

		var owlHeight = $activeSlider.height();
		if(owlHeight && owlHeight > 0){
			var parentHeight = $('#lightbox-toys').height() - $('div.featherlight-content div.menu').height();
			var dif = (parentHeight-owlHeight) * 0.45;
			$activeSlider.css({top: dif});
		}

	}


})(jQuery, this);


/**
 * @link https://davidwalsh.name/javascript-debounce-function
 *
 * @param func
 * @param wait
 * @param immediate
 * @return {Function}
 */
function debounce(func, wait, immediate) {
	var timeout;
	return function () {
		var context = this, args = arguments;
		var later = function () {
			timeout = null;
			if (!immediate) {
				func.apply(context, args);
			}
		};
		var callNow = immediate && !timeout;
		clearTimeout(timeout);
		timeout = setTimeout(later, wait);
		if (callNow) {
			func.apply(context, args);
		}
	};
};
